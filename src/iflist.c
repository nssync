/* This file is part of NSsync 
   Copyright (C) 2011-2017 Sergey Poznyakoff
  
   NSsync is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   NSsync is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with NSsync.  If not, see <http://www.gnu.org/licenses/>. */

#include "nssync.h"
#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <arpa/inet.h>

struct sockaddr_in *hostaddr;
size_t addr_count;

void
get_host_addresses()
{
#ifdef SIOCGIFADDR
	struct if_nameindex *idx;
        size_t i, j;
	struct ifreq ifr;
	int fd;

	idx = if_nameindex();
	if (!idx) {
		error("can't get list of interfaces: %s", strerror(errno));
		exit(EX_UNAVAILABLE);
	}
	
		
	for (addr_count = 0; idx[addr_count].if_index > 0; addr_count++)
		;

	if (addr_count) {
		fd = socket(AF_INET, SOCK_DGRAM, 0);
		if (fd == -1) {
			error("can't open socket: %s", strerror(errno));
			exit(EX_UNAVAILABLE);
		}

		hostaddr = grecs_calloc(addr_count, sizeof(hostaddr[0]));
		for (i = j = 0; i < addr_count; i++) {
			size_t len = strlen(idx[i].if_name);
			if (len >= sizeof(ifr.ifr_name)) {
				error("interface name too long: %s",
				      idx[i].if_name);
				continue;
			}
			strcpy(ifr.ifr_name, idx[i].if_name);
			if (ioctl(fd, SIOCGIFADDR, &ifr) == -1) {
				if (errno == EADDRNOTAVAIL)
					debug(2, ("%s: no address assigned",
						  idx[i].if_name));
				else
					error("ioctl failed for %s: %s",
					      idx[i].if_name, strerror(errno));
				continue;
			}
			memcpy(&hostaddr[j], &ifr.ifr_addr,
			       sizeof(struct sockaddr_in));
			++j;
		}
		addr_count = j;
		close(fd);
	}
	
	if_freenameindex(idx);
#else
	error("check-ns is not supported on this host");
	exit(EX_UNAVAILABLE);
#endif
}
	
int
is_my_sockaddr(struct sockaddr_in *s)
{
	size_t i;
	
	for (i = 0; i < addr_count; i++)
		if (memcmp(hostaddr + i, s, sizeof(*s)) == 0)
			return 1;
	return 0;
}
