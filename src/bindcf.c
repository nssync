/* This file is part of NSsync 
   Copyright (C) 2011-2017 Sergey Poznyakoff
  
   NSsync is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   NSsync is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with NSsync.  If not, see <http://www.gnu.org/licenses/>. */

#include "nssync.h"
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

char *named_conf_file;
struct grecs_list *bind_include_path;
char *zone_file_pattern = "$zone.$synctag";
char *zone_conf_file;

char *bind_working_dir;
static struct grecs_node *bind_tree;

struct filesym {
	const char *name;
	struct grecs_node *node;
};
static struct grecs_symtab *filetab;

static struct grecs_node *
find_file(const char *name, struct grecs_node *node)
{
	int install = 1;
	struct filesym key, *ret;
	if (!filetab) {
		filetab = grecs_symtab_create_default(sizeof(struct filesym));
		if (!filetab)
			grecs_alloc_die();
	}
	key.name = name;
	ret = grecs_symtab_lookup_or_install(filetab, &key, &install);
	if (!ret)
		grecs_alloc_die();
	if (install) {
		ret->node = node;
		return NULL;
	}
	return ret->node;
}

void
filetab_clear(void)
{
	grecs_symtab_clear(filetab);
}

void
source_named_conf(void)
{
	struct grecs_list_entry *ep;
	struct grecs_node *node;
	
	grecs_include_path_clear();
	if (bind_include_path) {
		struct grecs_list_entry *ep;
		
		for (ep = bind_include_path->head; ep; ep = ep->next)
			grecs_preproc_add_include_dir(grecs_strdup(ep->data));
	}
		
	grecs_parser_fun = grecs_bind_parser;
	if (!named_conf_file)
		named_conf_file = "/etc/named.conf";
			
	bind_tree = grecs_parse(named_conf_file);
	if (!bind_tree)
		exit(EX_UNAVAILABLE);

	node = grecs_find_node(bind_tree, ".options.directory");
	if (node) {
		if (node->type != grecs_node_stmt ||
		    !node->v.value ||
		    node->v.value->type != GRECS_TYPE_STRING) {
			grecs_error(&node->locus, 0,
				    "suspicious directory statement");
			exit(EX_CONFIG);
		} else
			bind_working_dir = node->v.value->v.string;
	}
	
	if (!bind_working_dir) {
		if (bind_include_path) {
			bind_working_dir = bind_include_path->head->data;
			error("warning: bind working dir not set; "
			      "assuming %s", bind_working_dir);
		}
		
		if (!bind_working_dir) {
			error("cannot determine directory name for generated"
			      " zone files; set zonefile-dir to fix");
			exit(EX_CONFIG);
		}
	}
	
	for (ep = synclist->head; ep; ep = ep->next) {
		struct nssync *sp = ep->data;
		struct grecs_node *np;

		if (strcmp(named_conf_file, sp->zone_conf_file) == 0) {
			error("zone-conf cannot be the same as named-conf, "
			      "in %s", sp->tag);
			exit(EX_CONFIG);
		}
		if (access(sp->zone_conf_file, F_OK) && errno == ENOENT) {
			sp->zone_tree = grecs_node_create(grecs_node_root,
							  NULL);
		} else
			sp->zone_tree = grecs_parse(sp->zone_conf_file);
		if (!sp->zone_tree)
			exit(EX_UNAVAILABLE);
		for (np = sp->zone_tree->down; np; np = np->next) {
			if (strcmp(np->ident, "zone")) {
				grecs_error(&np->locus, 0,
					   "only zone statements are allowed");
				exit(EX_CONFIG);
			}
		}
	}
}

static char *
absolute_name(const char *name, const char *dir)
{
	char *p;
	
	if (name[0] == '/')
		return grecs_strdup(name);

	p = grecs_find_include_file(name, 1);
	if (!p) {
		size_t s = 0;
		if (grecs_asprintf(&p, &s, "%s/%s", dir, name))
			grecs_alloc_die();
	}
	return p;
}

static const char *
strip_bind_working_dir(const char *name)
{
	if (bind_working_dir) {
		size_t len = strlen(bind_working_dir);
		if (strncmp(name, bind_working_dir, len) == 0) {
			if (bind_working_dir[len-1] == '/')
				return name + len;
			else if (name[len] == '/')
				return name + len + 1;
		}
	}
	return name;
}

static struct grecs_node *
nssync_node_create(struct nssync *sp,
		   enum grecs_node_type type,
		   grecs_locus_t *loc)
{
	struct grecs_node *node = grecs_node_create(type, loc);
	grecs_list_append(sp->new_nodes, node);
	return node;
}
	    
static void
new_zone(struct nssync *sp, const char *zone, char *file)
{
	struct grecs_node *node, *np;
#define fake_locus(n) do {			\
		(n)->locus.beg.file = (n)->locus.end.file = __FILE__;	\
		(n)->locus.beg.line = (n)->locus.end.line = __LINE__;	\
	} while (0)
	
	debug(1, ("%s: creating new zone file for %s, file %s", sp->tag,
		  zone, file));

	node = nssync_node_create(sp, grecs_node_block, NULL);
	node->ident = grecs_strdup("zone");
	node->v.value = grecs_malloc(sizeof(node->v.value[0]));
	node->v.value->type = GRECS_TYPE_STRING;
	node->v.value->v.string = grecs_strdup(zone);
	fake_locus(node);
	
	np = nssync_node_create(sp, grecs_node_stmt, NULL);
	np->ident = grecs_strdup("file");
	np->v.value = grecs_malloc(sizeof(np->v.value[0]));
	np->v.value->type = GRECS_TYPE_STRING;
	np->v.value->v.string = grecs_strdup(file);
	fake_locus(np);
	
	node->down = np;
	
	np = nssync_node_create(sp, grecs_node_stmt, NULL);
	np->ident = grecs_strdup("$found$");
	fake_locus(np);
	grecs_node_bind(node, np, 1);

	grecs_node_bind(sp->zone_tree, node, 1);
#undef fake_locus
}	

#define is_safe_char(c) \
 (isascii(c) && (isalnum(c) || strchr("_.-", (c)) != NULL))

static char *
safe_filename(const char *file)
{
	size_t size = 0;
	const char *p;
	char *name, *q;
		
	for (p = file; *p; p++) {
		if (is_safe_char(*p))
			size++;
		else
			size += 3;
	}
	if (size == strlen(file))
		return grecs_strdup(file);
	
	name = grecs_malloc(size + 1);
	for (p = file, q = name; *p; p++) {
		if (is_safe_char(*p))
			*q++ = *p;
		else {
			unsigned char n;
			
			*q++ = '%';
			n = ((unsigned char)*p) / 16;
			*q++ = n > 10 ? n + 'A' : n + '0';
			n = ((unsigned char)*p) % 16;
			*q++ = n > 10 ? n + 'A' : n + '0';
		}
	}
	*q = 0;
	return name;
}

static char *
bindcf_zone_name(struct nssync *sp, const char *zone)
{
	char *name;
	char *env[5];
	struct wordsplit ws;
	
	env[0] = "zone";
	env[1] = safe_filename(zone);
	env[2] = "synctag";
	env[3] = sp->tag;
	env[4] = NULL;
	ws.ws_env = (const char**)env;
	if (wordsplit(sp->zone_file_pattern, &ws,
		      WRDSF_NOCMD | WRDSF_ENV | WRDSF_ENV_KV |
		      WRDSF_NOSPLIT | WRDSF_KEEPUNDEF)) {
		error("cannot split zone-file-pattern: %s",
		      wordsplit_strerror(&ws));
		exit(EX_SOFTWARE);
	}
	grecs_free(env[1]);
	name = absolute_name(ws.ws_wordv[0], bind_working_dir);
	wordsplit_free(&ws);
	return name;
}

char *
bindcf_lookup(struct nssync *sp, const char *zone)
{
	char *name;
	struct grecs_node *node;

	for (node = sp->zone_tree->down; node; node = node->next)
		if (node->type == grecs_node_block &&
		    strcmp(node->ident, "zone") == 0 &&
		    node->v.value &&
		    node->v.value->type == GRECS_TYPE_STRING &&
		    strcasecmp(node->v.value->v.string, zone) == 0)
			break;

	name = bindcf_zone_name(sp, zone);
	
	if (node) {
		char *oname;
		struct grecs_node *np = grecs_find_node(node->down, "file");
		if (!np) {
			dlz_error(zone,
				  "%s:%d: no file statement in zone!",
				  node->locus.beg.file, node->locus.beg.line);
			return NULL;
		}
		if (np->type != grecs_node_stmt ||
		    !np->v.value ||
		    np->v.value->type != GRECS_TYPE_STRING) {
			grecs_error(&np->locus, 0,
				    "suspicious file statement");
			dlz_error(zone, "suspicious file statement");
			return NULL;
		}
		oname = absolute_name(np->v.value->v.string, bind_working_dir);
		if (strcmp(oname, name)) {
			debug(1, ("renaming zone file %s to %s", oname, name));
			if (move_file(oname, name)) {
				free(oname);
				free(name);
				dlz_error(zone, "cannot rename file");
				return NULL;
			}
			grecs_free(np->v.value->v.string);
			np->v.value->v.string = name;
			name = grecs_strdup(np->v.value->v.string);
		}
			
		np = nssync_node_create(sp, grecs_node_stmt, NULL);
		np->ident = grecs_strdup("$found$");
		grecs_node_bind(node, np, 1);
		return name;
	}

	/* No zone statement: new zone */
	name = bindcf_zone_name(sp, zone);
	new_zone(sp, zone, name);
	return name;
}

static void
output_zone(FILE *fp, struct grecs_node *node, struct nssync *sp)
{
	struct grecs_node *np;
	
	fprintf(fp, "zone \"%s\" {\n", node->v.value->v.string);
	fprintf(fp, "  type master;\n");
	np = grecs_find_node(node->down, "file");
	if (!np)
		abort();
	fprintf(fp, "  file \"%s\";\n",
		strip_bind_working_dir(np->v.value->v.string));
	if (sp->zone_add_stmt)
		fprintf(fp, "%s", sp->zone_add_stmt);
	fprintf(fp, "};\n");
}

void
flush_zone_list(struct nssync *sp)
{
	char *fname= NULL;
	size_t size = 0;
	mode_t um;
	struct grecs_node *node;
	int fd;
	FILE *fp;
	unsigned long zone_count = 0;
	
	debug(1, ("%s: creating temporary zone list", sp->tag));
	if (grecs_asprintf(&fname, &size,
			   "%s/nssyncXXXXXX", tempdir))
		grecs_alloc_die();

	um = umask(077);
	fd = mkstemp(fname);
	umask(um);
	
	if (fd == -1) {
		error("cannot create temporary file %s: %s",
		      fname, strerror(errno));
		exit(EX_UNAVAILABLE);
	}
	fp = fdopen(fd, "w+");
	
	fprintf(fp, "# Bind configuration include file generated by %s\n",
		PACKAGE_STRING);
	fprintf(fp, "# Do not edit! See %s for details\n", config_file);
	for (node = sp->zone_tree->down; node; node = node->next) {
		struct grecs_node *file_node =
			grecs_find_node(node->down, "file");
		char *file = NULL;
		struct grecs_node *np = NULL;
		
		if (file_node) {
			file = absolute_name(file_node->v.value->v.string,
					     bind_working_dir);
			np = find_file(file, file_node);
		}

		if (grecs_find_node(node->down, "$found$")) {
			if (np) {
				int fd;
				char *new_name = NULL;
				size_t size = 0;

				if (grecs_asprintf(&new_name, &size,
						   "%s.XXXXXX",
						file_node->v.value->v.string))
					grecs_alloc_die();
				fd = mkstemp(new_name);
				if (fd == -1) {
					grecs_error(&node->locus, 0,
						    "can't create temporary: "
						    "%s",
						    strerror(errno));
					dlz_error(node->v.value->v.string,
						  "can't create temporary");
					continue;
				}
				close(fd);
				grecs_warning(&node->locus, 0,
					      "file %s already in use; "
					      "using %s instead",
					      file_node->v.value->v.string,
					      new_name);
				grecs_warning(&np->locus, 0,
					      "this is the location of "
					      "the previous definition");
				grecs_free(file_node->v.value->v.string);
				file_node->v.value->v.string = new_name;
			}
			output_zone(fp, node, sp);
			zone_count++;
		} else if (!file_node) {
			error("about to delete zone %s, which has "
			      "no file definition",
			      node->v.value->v.string);
		} else if (file && !np) {
			debug(1, ("deleting zone %s, file %s",
				  node->v.value->v.string,
				  file));
			if (!dry_run_mode && unlink(file)) {
				error("unlink(%s): %s",
				      file, strerror(errno));
			}
		}
	}

	debug(1, ("number of zones: %lu", zone_count));

	fflush(fp);
	if (compare(fname, sp->zone_conf_file)) {
		debug(1, ("recreating %s", sp->zone_conf_file));
		if (copy_file(fname, fp, sp->zone_conf_file))
			dlz_error(NULL,
				  "can't create zone configuration file");
	}
	fclose(fp);
	unlink(fname);
	free(fname);

	/* Cleanup */
	grecs_list_clear(sp->new_nodes);
}
