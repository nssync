/* This file is part of NSsync 
   Copyright (C) 2011-2017 Sergey Poznyakoff
  
   NSsync is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   NSsync is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with NSsync.  If not, see <http://www.gnu.org/licenses/>. */

#include "nssync.h"

char *sql_config_file;
char *sql_config_group;
char *database_name;
char *sql_host;
char *sql_socket;
char *sql_user;
char *sql_password;
char *sql_cacert;
int sql_port;

MYSQL mysql;

void
sql_connect()
{
	mysql_init(&mysql);

	if (sql_config_file)
		mysql_options(&mysql, MYSQL_READ_DEFAULT_FILE,
			      sql_config_file);
	if (sql_config_group)
		mysql_options(&mysql, MYSQL_READ_DEFAULT_GROUP,
			      sql_config_group);
      
	if (sql_cacert)
		mysql_ssl_set(&mysql, NULL, NULL, sql_cacert, NULL, NULL);
	if (!mysql_real_connect(&mysql, sql_host, sql_user,
				sql_password, database_name, sql_port,
				sql_socket, CLIENT_MULTI_RESULTS)) {
		error("failed to connect to database: %s",
		      mysql_error(&mysql));
		exit(EX_UNAVAILABLE);
	}
}

void
sql_disconnect()
{
	mysql_close(&mysql);
}

int
sql_do_query(const char *query, 
	     int (*fun)(MYSQL_ROW, unsigned, void*), void *data)
{
	if (mysql_query(&mysql, query)) {
		error("%s", mysql_error(&mysql));
		error("in query: %s", query);
		return 1;
	}

	do {
		MYSQL_ROW row;
		MYSQL_RES *res;
		unsigned n;
		
		res = mysql_store_result(&mysql);
		if (!res) {
			error("cannot get result: %s", mysql_error(&mysql));
			error("in query: %s", query);
			return 1;
		}

		n = mysql_num_fields(res);
		
		while ((row = mysql_fetch_row(res)) != NULL)
			if (fun(row, n, data))
				break;
		mysql_free_result(res);
	} while (mysql_next_result(&mysql) == 0);

	return 0;
}


struct slave_status_info
{
	char *file;
	char *off;
};

static int
read_slave_status(MYSQL_ROW row, unsigned nf, void *closure)
{
	struct slave_status_info *ssi = closure;
	ssi->file = grecs_strdup(row[5]);
	ssi->off = grecs_strdup(row[6]);
	return 1;
}

int
sql_get_slave_status(char **pfile, char **poff)
{
	struct slave_status_info ssi = { NULL, NULL };
	if (sql_do_query("SHOW SLAVE STATUS", read_slave_status, &ssi))
		return 1;
	if (!ssi.file || !ssi.off)
		return 1;
	*pfile = ssi.file;
	*poff = ssi.off;
	return 0;
}


	
