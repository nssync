/* This file is part of NSsync 
   Copyright (C) 2011-2017 Sergey Poznyakoff
  
   NSsync is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   NSsync is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with NSsync.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sysexits.h>
#include <errno.h>

#include <grecs.h>
#include "grecs/json.h"
#include <wordsplit.h>
#include <mysql/mysql.h>

extern int lint_mode;
extern int dry_run_mode;
extern int preprocess_only;
extern int debug_level;
extern char *config_file;
extern char *slave_status_file;
extern char *pidfile;
extern char *tempdir;
extern char *reload_command;
extern int check_ns;
extern char *runas_user;
extern char *runas_group;
extern int syslog_facility;
extern char *syslog_tag;
extern int foreground;

extern struct json_value *changed_zones;
extern struct json_value *error_list;

extern struct grecs_sockaddr *server_addr;
#ifndef DEFAULT_NSSYNC_ADDR
# define DEFAULT_NSSYNC_ADDR "127.0.0.1:8080"
#endif
extern unsigned periodic_timeout;
extern unsigned delay_timeout;

extern char *sql_config_file;
extern char *sql_config_group;
extern char *database_name;
extern char *sql_host;
extern char *sql_socket;
extern char *sql_user;
extern char *sql_password;
extern char *sql_cacert;
extern int sql_port;

extern char *named_conf_file;
extern struct grecs_list *bind_include_path;

extern char *zone_file_pattern;
extern char *zone_conf_file;
extern char *bind_working_dir;

extern struct grecs_list *synclist;

extern int debug_level;

#define debug(n,s)							\
	do {								\
		if ((n) <= debug_level)					\
			((n) == 1 ? info_printf : debug_printf) s;	\
	} while (0)

#ifndef __PRINTFLIKE
# define __PRINTFLIKE(fmt,narg) __attribute__ ((__format__ (__printf__, fmt, narg)))
#endif

void config_init(void);
void config_parse(void);
void config_help(void);

void verror(const char *fmt, va_list ap);
void error(const char *fmt, ...) __PRINTFLIKE(1,2);
void debug_printf(const char *fmt, ...) __PRINTFLIKE(1,2);
void info_printf(const char *fmt, ...) __PRINTFLIKE(1,2);
void start_syslog(void);

void runas(void);
void create_pidfile(void);

void set_signal(int sig, void (*sighandler)(int));
void set_signals(int *sigv, void (*sighandler)(int));
extern int fatal_signals[];

void dlz_error(char const *zone, char const *fmt, ...);
void dlz_success(char const *zone);

void sql_connect(void);
void sql_disconnect(void);
int sql_do_query(const char *query, 
		 int (*fun)(MYSQL_ROW, unsigned, void*), void *data);
int sql_get_slave_status(char **pfile, char **poff);

void get_host_addresses(void);
struct sockaddr_in;
int is_my_sockaddr(struct sockaddr_in *s);

struct nsdef {
	struct nsdef *next;
	char *type;
	char *data;
};

struct nssync {
	char *tag;
	char *zone_conf_file;
	char *zone_file_pattern;
	char *zone_add_stmt;
	struct grecs_node *zone_tree;
	struct grecs_list *new_nodes;
	
	char *soa_query;
	char *rr_query;
	char *rev_rr_query;
	char *ns_query;

	struct nsdef *nsdef_head, *nsdef_tail;
	int myzone;
	
	char *file_name;
	char *temp_file_name;
	FILE *fp;
};

void format_zones(struct nssync *);

void source_named_conf(void);
char *bindcf_lookup(struct nssync *sp, const char *zone);
void flush_zone_list(struct nssync *sp);

int compare(const char *oldfile, const char *newfile);
int move_file(const char *file, const char *dst_file);
int copy_file(const char *file_name, FILE *infile, const char *dst_file);

void filetab_clear(void);

int nssync_server(void);
void nssync_reschedule(int);
void *nssync_timer(void *arg);
char *nssync_format_result(void);

int nssync(struct json_value **ret_json);


	
