/* This file is part of NSsync 
   Copyright (C) 2017 Sergey Poznyakoff
  
   NSsync is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   NSsync is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with NSsync.  If not, see <http://www.gnu.org/licenses/>. */

#include "nssync.h"
#include <pthread.h>

static pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
static struct timespec wakets;
static int wakets_set;
static int wake_up;
static struct json_value *last_result;
static pthread_mutex_t result_mtx = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t result_cond = PTHREAD_COND_INITIALIZER;

static void
getts(struct timespec *ts)
{
	if (wakets_set) {
		*ts = wakets;
		wakets_set = 0;
	} else {
		struct timespec now;
		clock_gettime(CLOCK_REALTIME, &now);
		
		ts->tv_sec = now.tv_sec - now.tv_sec % periodic_timeout
			     + periodic_timeout;
		ts->tv_nsec = 0;
	}
}	

static void
wait_result(void)
{
	pthread_mutex_lock(&result_mtx);
	while (wake_up)
		pthread_cond_wait(&result_cond, &result_mtx);
	pthread_mutex_unlock(&result_mtx);
}

static void
update_result(void)
{
	pthread_mutex_lock(&result_mtx);
	pthread_cond_signal(&result_cond);
	pthread_mutex_unlock(&result_mtx);
}

void
nssync_reschedule(int now)
{
	pthread_mutex_lock(&mtx);
	if (now) {
		wake_up = 1;
	} else {
		clock_gettime(CLOCK_REALTIME, &wakets);
		wakets.tv_sec += delay_timeout;
		wakets_set = 1;
	}
	pthread_cond_signal(&cond);
	pthread_mutex_unlock(&mtx);
	if (now)
		wait_result();
}

void *
nssync_timer(void *arg)
{
	pthread_mutex_lock(&mtx);
	while (1) {
		int rc;
		struct timespec ts;
		getts(&ts);
		if (debug_level >= 1) {
			char buf[26];
			ctime_r(&ts.tv_sec, buf);
			buf[24] = 0;
			debug_printf("sleeping till %s", buf);
		}
		if (rc = pthread_cond_timedwait(&cond, &mtx, &ts)) {
			if (rc != ETIMEDOUT) {
				error("pthread_cond_timedwait: %s",
				      strerror(errno));
			} else
				wake_up = 1;
		}
		if (wake_up) {
			debug(1,("wake up"));
			json_value_free(last_result);
			last_result = NULL;
			nssync(&last_result);			
			wake_up = 0;
			update_result();
		}
	}
	pthread_mutex_unlock(&mtx);
	return NULL;
}

static void
json_writer(void *closure, char const *text, size_t len)
{
	grecs_txtacc_t acc = closure;
	grecs_txtacc_grow(acc, text, len);
}

char *
nssync_format_result(void)
{
	struct json_format fmt;
	grecs_txtacc_t acc;
	char *buf;

	pthread_mutex_lock(&mtx);

	acc = grecs_txtacc_create();
	fmt.indent = 0;
	fmt.precision = 0;
	fmt.write = json_writer;
	fmt.data = acc;
	json_format_value(last_result, &fmt);
	
	grecs_txtacc_grow_char(acc, 0);
	buf = grecs_txtacc_finish(acc, 1);
	grecs_txtacc_free(acc);
	
	pthread_mutex_unlock(&mtx);

	return buf;
}
