/* This file is part of NSsync 
   Copyright (C) 2011-2017 Sergey Poznyakoff
  
   NSsync is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   NSsync is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with NSsync.  If not, see <http://www.gnu.org/licenses/>. */

#include "nssync.h"
#include <inttypes.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <tcpd.h>
#include <signal.h>
#include <pthread.h>
#define MHD_PLATFORM_H
#include <microhttpd.h>

static int
setup_listen_socket(struct grecs_sockaddr *sa, int fd)
{
	if (bind(fd, sa->sa, sa->len) == -1) {
		error("%s: cannot bind: %s",
		      grecs_sockaddr_str(sa), strerror(errno));
		return -1;
	}

	if (listen(fd, 8) == -1) {
		error("%s: listen: %s",
		      grecs_sockaddr_str(sa), strerror(errno));
		return -1;
	}
	return 0;
}

static int
open_socket(struct grecs_sockaddr *sa)
{
	int reuse;
	int fd;

	fd = socket(sa->sa->sa_family, SOCK_STREAM, 0);
	if (fd == -1) {
		error("socket: %s", strerror(errno));
		return -1;
	}
	reuse = 1;
	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR,
		   &reuse, sizeof(reuse));

	if (setup_listen_socket(sa, fd)) {
		close(fd);
		return -1;
	}
	return fd;
}

static void
nssync_mhd_panic(void *cls, const char *file, unsigned int line,
		 const char *reason)
{
	if (reason)
		error("%s:%d: MHD PANIC: %s", file, line, reason);
	else
		error("%s:%d: MHD PANIC", file, line);
	abort();
}

static void
nssync_mhd_logger(void *arg, const char *fmt, va_list ap)
{
	verror(fmt, ap);
}

static int
nssync_mhd_acl(void *cls, const struct sockaddr *addr,  socklen_t addrlen)
{
	struct request_info req;
	request_init(&req,
		     RQ_DAEMON, "nssync",
		     RQ_CLIENT_SIN, addr,
		     RQ_SERVER_SIN, cls,
		     NULL);
	sock_methods(&req);
	return hosts_access(&req) ? MHD_YES : MHD_NO;
}

static int
cb_not_found(void *cls,
	     struct MHD_Connection *connection,
	     const char *url, const char *method,
	     const char *version,
	     const char *upload_data, size_t *upload_data_size,
	     void **con_cls)
{
	int ret;
	struct MHD_Response *response;
	static char errtext[] = "Not found";
	
	response = MHD_create_response_from_buffer (strlen(errtext),
						    (void *) errtext,
						    MHD_RESPMEM_PERSISTENT);
	ret = MHD_queue_response(connection, MHD_HTTP_NOT_FOUND, response);
	MHD_destroy_response(response);
	return ret;
}

static int
bad_request(struct MHD_Connection *connection)
{
	int ret;
	struct MHD_Response *response;
	static char errtext[] = "bad request";
	
	response = MHD_create_response_from_buffer (strlen(errtext),
						    (void *) errtext,
						    MHD_RESPMEM_PERSISTENT);
	ret = MHD_queue_response(connection, MHD_HTTP_BAD_REQUEST, response);
	MHD_destroy_response(response);
	return ret;
}


static int
cb_bad_method(void *cls,
	      struct MHD_Connection *connection,
	      const char *url, const char *method,
	      const char *version,
	      const char *upload_data, size_t *upload_data_size,
	      void **con_cls)
{
	int ret;
	struct MHD_Response *response;
	
	response = MHD_create_response_from_buffer (0,
						    "",
						    MHD_RESPMEM_PERSISTENT);
	ret = MHD_queue_response(connection, MHD_HTTP_METHOD_NOT_ALLOWED, response);
	MHD_destroy_response(response);
	return ret;
}

static int
do_sync(void *cls,
	struct MHD_Connection *connection,
	const char *url, const char *method,
	const char *version,
	const char *upload_data, size_t *upload_data_size,
	void **con_cls)
{
	int ret;
	struct MHD_Response *response;
	char *buf;
	grecs_txtacc_t acc = *con_cls;
	
	if (*url)
		return cb_not_found(cls, connection, url, method,
				    version, upload_data, upload_data_size,
				    con_cls);

	if (strcmp(method, MHD_HTTP_METHOD_GET) == 0) {
		/* Nothing */;
	} else if (strcmp(method, MHD_HTTP_METHOD_POST) == 0) {
		if (!acc) {
			char const *t = MHD_lookup_connection_value(connection,
						 MHD_HEADER_KIND,
						 MHD_HTTP_HEADER_CONTENT_TYPE);
			if (t && strcmp(t, "application/json"))
				return bad_request(connection);
			acc = grecs_txtacc_create();
			*con_cls = acc;
			return MHD_YES;
		}
		if (*upload_data_size) {
			grecs_txtacc_grow(acc, upload_data, *upload_data_size);
			*upload_data_size = 0;
			return MHD_YES;
		} else {
			/* Done with the data, serve the request now */
			char *p;
			struct json_value *json;

			grecs_txtacc_grow_char(acc, 0);
			p = grecs_txtacc_finish(acc, 0);
			
			if (*p) {
				json = json_parse_string(p, strlen(p));
			} else
				json = json_new_null();

			grecs_txtacc_free(acc);			
			con_cls = NULL;

			if (!json)
				return bad_request(connection);

			if (json->type == json_null) {
				nssync_reschedule(0);
			} else if (json->type == json_object) {
				struct json_value *v;
				if (json_object_get(json, "wait", &v) == 0
				    && v->type == json_bool) {
					nssync_reschedule(v->v.b);
				}
			}
			json_value_free(json);
		}
	} else
		return cb_bad_method(cls, connection, url, method,
				     version, upload_data, upload_data_size,
				     con_cls);

	buf = nssync_format_result();
	response = MHD_create_response_from_buffer (strlen(buf),
						    (void *) buf,
						    MHD_RESPMEM_MUST_FREE);
	MHD_add_response_header(response, "Content-Type", "application/json");

	ret = MHD_queue_response(connection, MHD_HTTP_OK, response);
	MHD_destroy_response(response);
	return ret;
}

struct nssync_resource {
	char const *uri;
	size_t uri_len;
	MHD_AccessHandlerCallback cbf;
	int (*predicate)(void);
};

static struct nssync_resource nssync_resources[] = {
#define S(s) #s, (sizeof(#s) - 1)
	{ S(nssync), do_sync },
#undef S
	{ NULL }
};

static MHD_AccessHandlerCallback
find_callback(const char **puri)
{
	const char *endpoint = *puri + 1;
	struct nssync_resource *p;
	size_t len = strcspn(endpoint, "?");

	if (len == 0)
		return cb_not_found;

	if (endpoint[len-1] == '/')
		--len;
	*puri = endpoint + len;

	for (p = nssync_resources; p->uri; p++)
		if (len >= p->uri_len
		    && memcmp(p->uri, endpoint, p->uri_len) == 0
		    && (endpoint[p->uri_len] == 0
			|| endpoint[p->uri_len] == '/'
			|| endpoint[p->uri_len] == '?')
		    && (!p->predicate || p->predicate()))
			return p->cbf;
	return cb_not_found;
}

static int
nssync_mhd_handler(void *cls,
		   struct MHD_Connection *conn,
		   const char *url, const char *method,
		   const char *version,
		   const char *upload_data, size_t *upload_data_size,
		   void **con_cls)
{
	MHD_AccessHandlerCallback cb;

	cb = find_callback(&url);
	return cb(cls, conn, url, method, version, upload_data,
		  upload_data_size, con_cls);
}

int
nssync_server(void)
{
	int fd;
	struct MHD_Daemon *mhd;
	sigset_t sigs;
	int i;
	int sn;
	pthread_t thr;
	
	if (!server_addr) {
		if (grecs_str_to_sockaddr(&server_addr, DEFAULT_NSSYNC_ADDR,
					  grecs_sockaddr_hints, NULL))
			return EX_UNAVAILABLE;
	}
	
	fd = open_socket(server_addr);
	if (fd == -1)
		return EX_UNAVAILABLE;

	runas();
	
	if (!foreground) {
		int i;
		if (daemon(0, 1)) {
			error("daemon failed: %s", strerror(errno));
			return EX_UNAVAILABLE;
		}
		for (i = 0; i < fd; i++)
			close(i);
		start_syslog();
	}
	create_pidfile();
	debug(1,("start up"));

	/* Block the 'fatal signals' and SIGPIPE in the handling thread */
	sigemptyset(&sigs);
	for (i = 0; fatal_signals[i]; i++)
		sigaddset(&sigs, fatal_signals[i]);
	sigaddset(&sigs, SIGPIPE);
	pthread_sigmask(SIG_BLOCK, &sigs, NULL);

	MHD_set_panic_func(nssync_mhd_panic, NULL);
	mhd = MHD_start_daemon(MHD_USE_INTERNAL_POLLING_THREAD
			       | MHD_USE_ERROR_LOG, 0,
			       nssync_mhd_acl, server_addr,
			       nssync_mhd_handler, NULL,
			       MHD_OPTION_LISTEN_SOCKET, fd,
			       MHD_OPTION_EXTERNAL_LOGGER, nssync_mhd_logger, NULL,
			       MHD_OPTION_END);

	pthread_create(&thr, NULL, nssync_timer, NULL);
	
	/* Unblock only the fatal signals */
	sigdelset(&sigs, SIGPIPE);	
	pthread_sigmask(SIG_UNBLOCK, &sigs, NULL);
	/* Wait for signal to arrive */
	sigwait(&sigs, &sn);
	MHD_stop_daemon(mhd);

	return 0;
}
