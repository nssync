.\" This file is part of Nssync. -*- nroff -*-
.\" Copyright (C) 2011-2017 Sergey Poznyakoff
.\"
.\" Nssync is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Nssync is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with Nssync.  If not, see <http://www.gnu.org/licenses/>.
.TH NSSYNC "8" "August 17, 2017" "NSSYNC" ""
.SH NAME
\fBnssync\fR \- A DNS Zone File Maintenance Utility

.SH SYNOPSIS
\fBnssync\fR\
 [\fB\-EFSVXdfhnstx\fR]\
 [\fB\-D\fR \fISYMBOL[=VALUE\fR]]\
 [\fB\-I\fR \fIDIR\fR]\
 [\fB\-c\fR \fIFILE\fR]\
 [\fB\-\-config\-file=\fIFILE\fR]\
 [\fB\-\-config\-help\fR]\
 [\fB\-\-cron\fR]\
 [\fB\-\-debug\fR]\
 [\fB\-\-debug\-lexer\fR]\
 [\fB\-\-debug\-parser\fR]\
 [\fB\-\-define=\fISYMBOL\fR[=\fIVALUE\fR]]\
 [\fB\-\-dry\-run\fR]\
 [\fB\-\-force\fR]\
 [\fB\-\-foreground\fR]\
 [\fB\-\-help\fR]\
 [\fB\-\-include\-directory=\fIDIR\fR]\
 [\fB\-\-lint\fR]\
 [\fB\-\-no\-preprocessor\fR]\
 [\fB\-\-preprocessor=\fICOMMAND\fR]\
 [\fB\-\-server\fR]\
 [\fB\-\-syslog\fR]\
 [\fB\-\-usage\fR]\
 [\fB\-\-version\fR]
.SH WARNING
This manpage is a short description of the \fBnssync\fR utility.
For a detailed discussion, including examples of the configuration and
usage recommendation, refer to the \fBNssync User Manual\fR available in
Texinfo format.  To access it, run:

  \fBinfo nssync\fR

Should any discrepancies occur between this manpage and the
\fBNssync User Manual\fR, the later shall be considered the authoritative
source.
.SH DESCRIPTION
BIND, the most frequently used DNS server, normally keeps its zone
data in plain text files. This approach becomes inconvenient when the
number of zones grows beyond a certain limit. When this happens, the
obvious solution is to move all data to a database and make named read
it from there, and the recent versions of BIND are able to do so by
using dynamically loadable zones (DLZ).  However, DLZ has problems of
its own, one of them being that it is unable to propagate glue records. 

The nssync utility provides an alternative solution, which makes it
possible to keep your zone data in an SQL database without using DLZ
and with glue records working.
 
It does so by polling the database to determine records that have
recently changed and converting the database into BIND zone 
files.

The program can be run as a cron job, or as a standalone daemon.  In
the latter case, it listens on a specified address for a specific HTTP
request and triggers zone updates upon receiving it.  While listening,
it will also wake up periodically to run unconditional synchronizations.
.SH OPTIONS
.TP
\fB\-E\fR
Preprocess configuration file and exit.
.TP
\fB\-c\fR \fIFILE\fR, \fB\-\-config\-file=\fIFILE\fR
Use \fIFILE\fR instead of the default configuration file.
.TP
\fB\-\-cron\fR
Operate in cron mode.  This is the default.
.TP
\fB\-f\fR, \fB\-\-force\fR
Proceed even if SQL slave status has not changed.
.TP
\fB\-F\fR, \fB\-\-foreground\fR
Remain in foreground when running in server mode.
Valid only when used together with the \fB\-\-server\fR option.
.TP
\fB\-n\fR, \fB\-\-dry\-run\fR
Do nothing, print almost everything; implies
\fB\-\-debug.  Use additional \fB\-\-debug\fR options to get
even more info.
.TP
\fB\-t\fR, \fB\-\-lint\fR
Parse configuration file and exit.  The return status is 0 if the
syntax is OK, and 78 if errors were detected (see \fBEXIT CODES\fR below).
.TP
\fB\-D\fR \fISYMBOL\fR=\fIVALUE\fR, \fB\-\-define\fR=\fISYMBOL\fR[=\fIVALUE\fR]
Define a preprocessor symbol.
.TP
\fB\-I \fIDIR\fR, \fB\-\-include\-directory\fR=\fIDIR\fR
Add preprocessor include directory.
.TP
\fB\-\-no\-preprocessor\fR
Disable preprocessing.
.TP
\fB\-\-preprocessor\fR=\fICOMMAND\fR
Use \fICOMMAND\fR instead of the default preprocessor.
.TP
\fB\-s\fR, \fB\-\-server\fR
Run in server mode.
.TP
\fB\-S\fR, \fB\-\-syslog\fR
Use syslog for diagnostics (default for server mode).
.TP
\fB\-d\fR, \fB\-\-debug\fR
Increase debug level.
.TP
\fB\-X\fR, \fB\-\-debug\-lexer\fR
Debug configuration file lexer.
.TP
\fB\-x\fR, \fB\-\-debug\-parser\fR
Debug configuration file parser.
.TP
\fB\-\-config\-help\fR
Show configuration file summary.
.TP
\fB\-V\fR, \fB\-\-version\fR
Print program version.
.TP
\fB\-h\fR, \fB\-\-help\fR
Give this help list.
.TP
\fB\-\-usage\fR
Give a short usage message.
.SH CONFIGURATION FILE
.SS General Settings
.TP
\fBcheck\-ns\fR \fIBOOL\fR;
If set to \fBtrue\fR, \fBnssync\fR will check the list of NS
servers prior to creating a zone file.  The file will be created only
if IPv4 address of one of the servers matches one of the IP addresses
of the host on which \fBnssync\fR is run.
.TP
\fBpidfile\fR \fIFILE\fR;
Set the name of the PID file.  At startup, @command{nssync} checks if
the @var{file} already exists and is owned by an existing process.
If so, it exits.  The default PID file is
\fILOCALSTATEDIR\fB/nssync/\fIPROGNAME\fB.pid\fR, where \fIPROGNAME\fR
stands for the name under which the program was invoked and
\fILOCALSTATEDIR\fR is the name of the directory for modifiable
single-machine data set during configure.
.TP
\fBtempdir\fR \fIDIR\fR;
Sets the name for the temporary directory.  This is a directory where
\fBnssync\fR creates temporary zone files.  The argument must
point to an existing directory.
.TP
\fBnamed\-conf\fR \fIfile\fR;
Defines the full pathname of the \fBnamed\fR configuration file.
Default is \fB/etc/named.conf\fR.
.TP
\fBbind\-include\-path\fR \fILIST\fR;
Sets include search path for \fBinclude\fR directives found in BIND
configuration.  The argument is either a single directory or a list of
directories.
.TP
\fBzonefile\-pattern\fR \fIPAT\fR;
Defines the pattern for zone file names.  The name of each zone
file is created by expanding variable references in the \fIPAT\fR
argument.  The following variable references are defined: \fB$zone\fR
(or \fB${zone}\fR), which is replaced with the name of the zone
(without the trailing dot), and \fB$synctag\fR (or \fB${synctag}\fR),
replaced with the synchronization tag (see below).

Both notations (with and without braces) are equivalent.  The notation
with curly braces should be used if the reference is immediately
followed by a letter.

The default zone file pattern is \fB$zone.$synctag\fR.
.TP
\fBzone\-conf\fR \fIPAT\fR;
Defines the pattern for zone configuration file, i.e. a file
containing \fBzone\fR statements.

The handling of \fIPAT\fR is similar to that in \fBzonefile-pattern\fR,
except that only the \fB$synctag\fR reference is defined.
.TP
\fBreload\-command\fR \fIcmd\fR;
Defines a command to reload the nameserver.  The default is
\fB/usr/sbin/rndc reload\fR.
.TP
\fBuser\fR \fINAME\fR;
Run as unprivileged user \fINAME\fR.  If \fINAME\fR begins with a plus
sign, it is treated as a UID number of the user, instead of the user
name.

When switching to the user privileges, @command{nssync} retains the
supplementary groups of that user.
.TP
\fBgroup\fR \fINAME\fR;
Run with the privileges of group \fINAME\fR.  By default,
the primary group of the user supplied with the \fBuser\fR statement
is used.

If \fINAME\fR begins with a plus sign, it is treated as a GID number
of the group
.SS Server Settings
.EX
server {
    address [\fIIP\fR][fB:\fIPORT\fR];
    delay \fISECONDS\fR;
    wakeup \fISECONDS\fR;
}
.EE
.TP
\fBaddress\fR [\fIIP\fR][fB:\fIPORT\fR];
Specifies the IPv4 address and/or port to listen on for control requests.
Either IP or port parts can be omitted, but not both. In any case, the
colon before \fIPORT\fR is mandatory.  A host name can be used instead
of numeric IP, and a symbolic service name (from \fB/etc/services\fR),
in place of the numeric port.
.TP
\fBwakeup\fR \fISECONDS\fR;
Configures a periodic wake-up interval, in seconds.  This allows you
to run synchronizations periodically, while being able to perform them
on request.  The default wake-up interval is 3600 seconds.
.TP
\fBdelay\fR \fISECONDS\fR;
Configures the on-demand synchronization delay.  Upon receiving a
synchronization request, the server will wait this number of seconds
before actually satisfying it.  If another request arrives during this
interval, the prior one will be cancelled and the request rescheduled.
This helps avoid lots of spurious wake-ups that would otherwise occur
in case if several requests arrive in series within a short interval.
.SS Syslog configuration
.EX
syslog {
    facility \fINAME\fR;
    tag \fINAME\fR;
}
.EE
.TP
\fBfacility\fR \fINAME\fR;
Specifies the syslog facility to use.  Allowed names are:
.BR user ,
.BR daemon  " (default),"
.BR auth ,
.BR authpriv ,
.BR mail ,
.BR cron ,
.BR local0 " through " local7
(all names case-insensitive), or a facility number. 
.SS SQL Access
.EX
mysql {
    config-file \fIFILE\fR;
    config-group \fINAME\fR;
    database \fIDBNAME\fR;
    user \fINAME\fR;
    password \fITEXT\fR;
    ssl-ca \fIFILE\fR;
    slave-status-file \fIFILE\fR;
}
.EE
.TP
\fBhost\fR \fIHOSTNAME\fR[:\fIPORT\-OR\-SOCKET\fR];
Defines the SQL server IP and port.  The \fIHOSTNAME\fR can be either
the server IP address or its hostname.  The \fIPORT\-OR\-SOCKET\fR part,
if supplied, can be either the number of TCP port to use instead of
the default 3306 or the full pathname of the UNIX socket.  In the
latter case \fIHOSTNAME\fR is effectively ignored.
.TP
\fBdatabase\fR \fINAME\fR;
Sets the database name.
.TP
\fBssl\-ca\fR \fIFILE\fR;
Defines the name of the Certificate Authority (CA) file.
.TP
\fBuser\fR \fINAME\fR;
Sets SQL user name.
.TP
\fBpassword\fR \fIARG\fR;
Sets SQL user password.
.TP
\fBconfig\-file\fR \fIFILE\fR;
Read MySQL configuration from the \fBoption file\fR \fIFILE\fR. 
.TP
\fBconfig\-group\fR \fINAME\fR;
Read the named group from the SQL configuration file.
.TP
\fBslave\-status\-file\fR \fIFILE\fR;
Use this statement if \fBnssync\fR reads data from a slave
database.  It allows you to avoid recreating zone files if the
database information has not changed since the previous run.

If this statement is present, \fBnssync\fR will save the state of
the MySQL slave in \fIFILE\fR.  Upon startup, it will read these data
and compare them with the current state.  If they are the same, it
will exit immediately.
.SS Synchronization Block
A \fBsynchronization block\fR defines a set of zones to be
synchronized from the database and configures SQL statements which
return the zone data.  This set is identified by \fBsynchronization
tag\fR, supplied as the argument to the \fBsync\fR statement:

  # Define a synchronization block.
  \fBsync\fR \fITAG\fR {
    # zone configuration file
    \fBzone\-conf\fR \fIPAT\fR;
    # pattern for new zone file names
    \fBzonefile\-pattern\fR \fIPAT\fR;
    # add these statements to each generated zone file
    \fBadd\-statements\fR \fITEXT\fR;
    # a query for retrieving SOA records
    \fBsoa\-query\fR \fISTRING\fR;
    # a query for retrieving NS and similar records
    \fBns\-query\fR \fIstring\fR;
    # a query for retrieving the rest of RRs
    \fBrr\-query\fR \fIstring\fR;
    # a query for retrieving RRs from reverse delegation zones
    \fBrev\-rr\-query\fR \fIstring\fR;
  }

Statements within the \fBsync\fR block configure the zones:
.TP
\fBzone\-conf\fR \fIPAT\fR;
Defines the pattern for the name of zone configuration file for zones
in this synchronization block.  If not supplied, the global
\fBzone\-conf\fR statement will be used instead (see above).
.TP
\fBzonefile\-pattern\fR \fIPAT\fR;
Defines the pattern for zone file names.  If not supplied, the global
\fBzonefile\-pattern\fR statement will be used instead (see above).
.TP
\fBadd-statements\fR \fITEXT\fR;
Append \fITEXT\fR to each generated zone statement.  For example, the
following can be used to redefine forwarders and query ACLs for zones
in this synchronization block:

  add\-statements <<EOT
    forwarders { /* empty */ };
    allow\-query { local\-query\-only; };
  EOT;
.TP
The following statements define which zones pertain to this particular
synchronization block:
.TP
\fBsoa\-query\fR \fISTRING\fR;
A query for retrieving SOA records.
.TP
\fBns\-query\fR \fISTRING\fR;
A query for retrieving NS and similar records.  Use the \fB$zone\fR
reference for the zone name.
.TP
\fBrr\-query\fR \fISTRING\fR;
A query for retrieving the rest of RRs.  Use the \fB$zone\fR
reference for the zone name.
.TP
\fBrev\-rr\-query\fR \fISTRING\fR;
A query for retrieving RRs from reverse delegation zones.  Use the
\fB$zone\fR reference for the zone name.

.SH EXIT CODES
The following exit codes are defined:
.RS
.PD 0
.TP 3
.B 0
Normal termination.
.TP 3
.B 64
Invalid command line usage.
.TP 3
.B 69
Some error occurred.  For example, the program was unable to open
output file, etc.
.TP 3
.B 70
Internal software error.  This usually means hitting a bug in the
program, so please report it.
.TP 3
.B 98
Program terminated due to errors in configuration file.
.PD
.RE
If a non-0 code is returned the exact cause of failure is
reported on the currently selected logging channel.
.SH BUGS
Only MySQL is supported.
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <gray+nssync@gnu.org.ua>.
.SH COPYRIGHT
Copyright \(co 2011-2017 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH NSSYNC \"8\" \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 18
.\" end:


